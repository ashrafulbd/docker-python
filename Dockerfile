# Pull base image
FROM python:3.7

# Set environment varibles
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

COPY . /home/zoom/Documents/docker-python/dep
# Set work directory
WORKDIR /home/zoom/Documents/docker-python/dep


# Install dependencies
RUN pip install -r requirements.txt


# Run the app
RUN chmod +x run_app.sh
CMD ["./run_app.sh"]

EXPOSE 8000